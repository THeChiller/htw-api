"""This is the cli entry point for qs-server."""
from __future__ import annotations

import click
import uvicorn


@click.group()
def cli() -> None:
    pass


@cli.command()
def api() -> None:
    """Start the API."""
    uvicorn.run(
        "htw_api.api:api",
        host="localhost",
        port=8080,
    )
