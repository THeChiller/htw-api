"""This module containes shared dependencies for various routes."""
from __future__ import annotations

from typing import List

from htw_api.data.data import data
from htw_api.schemas.entry import Entry

parsed_data = list([Entry.parse_obj(entry) for entry in data])


def get_loaded_data() -> List[Entry]:
    return parsed_data
