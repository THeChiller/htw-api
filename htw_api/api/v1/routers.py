"""This module contains all routers for assembling an API."""

from fastapi import APIRouter

from htw_api.api.v1.endpoints import cities

v1_router = APIRouter()

# # include endpoints
v1_router.include_router(cities.router)
