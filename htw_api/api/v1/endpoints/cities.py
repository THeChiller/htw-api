"""This module contains endpoints for handling group."""
from __future__ import annotations

from typing import List, Optional

from fastapi import APIRouter, Depends
from pydantic import conint

from htw_api.api.v1.dependencies import get_loaded_data
from htw_api.schemas.entry import CitySearchEntry, Entry

router = APIRouter(prefix="/cities")


@router.get("/", response_model=List[CitySearchEntry])
def get_all_cities(
    search: str,
    limit: conint(ge=1, le=25) = 25,
    data: List[Entry] = Depends(get_loaded_data),
) -> List[Entry]:
    results = []
    for entry in data:
        if len(results) == limit:
            break
        if search.lower() in entry.UA_Name.lower():
            results.append(entry)

    return results


@router.get("/{city_id}", response_model=Optional[Entry])
def get_city_by_id(
    city_id: conint(ge=0),
    data: List[Entry] = Depends(get_loaded_data),
) -> Optional[Entry]:
    for entry in data:
        if city_id == entry.ID:
            return entry

    return None
