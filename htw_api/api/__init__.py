"""This module contains the service that handles frontend connections."""
from __future__ import annotations

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.routing import APIRoute

from htw_api.api.v1.routers import v1_router

origins = ["*"]


def use_route_names_as_operation_ids(app: FastAPI) -> None:
    """
    Simplify operation IDs so that generated API clients have simpler function names.

    Should be called only after all routes have been added.
    """
    for route in app.routes:
        if isinstance(route, APIRoute):
            route.operation_id = route.name


api = FastAPI(
    docs_url=f"/docs",
    openapi_url=f"/openapi.json",
    redoc_url=None,
)

api.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

api.include_router(v1_router)

use_route_names_as_operation_ids(api)
