from pydantic import BaseModel


class Entry(BaseModel):
    ID: int
    UA_Name: str
    UA_Country: str
    UA_Continent: str
    Housing: float
    Cost_of_Living: float
    Startups: float
    Venture_Capital: float
    Travel_Connectivity: float
    Commute: float
    Business_Freedom: float
    Safety: float
    Healthcare: float
    Education: float
    Environmental_Quality: float
    Economy: float
    Taxation: float
    Internet_Access: float
    Leisure_and_Culture: float
    Tolerance: float
    Outdoors: float


class CitySearchEntry(BaseModel):
    ID: int
    UA_Name: str
