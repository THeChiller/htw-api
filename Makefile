.DEFAULT_GOAL := build
.PHONY: install reformat

build:
	poetry build

install:
	poetry install

reformat:
	poetry run isort .
	poetry run black --line-length 88 .
